﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class CountryGroup
    { 

    }
    public class CountryGroupRequest
    {
        public int UserId {get;set;}
        public int Country_Group_Id { get;set;}
        public string Country_Group_Name {get;set;}
        public double Country_Group_Point {get;set;}
        public int Country_Group_Status { get; set; }

    }
    public class CountryRequest
    {
        public int UserId { get; set; }
        public int Country_Group_Id { get; set; }
        public int Country_Id { get; set; }
        public string Country_Name { get; set; }
        public int Country_Status { get; set; }

    }
}
