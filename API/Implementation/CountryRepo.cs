﻿using API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace API.Implementation
{
    public class CountryRepo
    {
        private IConfiguration Configuration;
        public CountryRepo(IConfiguration iconfiguration)
        {
            Configuration = iconfiguration;
        }
        public void ConnectionCheck()
        {
            string connString = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection connection = new SqlConnection(connString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
            }
        }

        public DataSet AddCountryGroup(CountryGroupRequest data)
        {
            DataSet ds = new DataSet();
            try
            {
                ConnectionCheck();
                SqlCommand dCmd = new SqlCommand("Add_Update_CountryGroup_SP", new SqlConnection(Configuration.GetConnectionString("DefaultConnection")));
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add(new SqlParameter("@UserId", data.UserId));
                dCmd.Parameters.Add(new SqlParameter("@Country_Group_Id", data.Country_Group_Id));
                dCmd.Parameters.Add(new SqlParameter("@Country_Group_Name", data.Country_Group_Name));
                dCmd.Parameters.Add(new SqlParameter("@Country_Group_Point", data.Country_Group_Point));
                dCmd.Parameters.Add(new SqlParameter("@Country_Group_Status", data.Country_Group_Status));
                using (SqlDataAdapter sda = new SqlDataAdapter(dCmd))
                {
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DataSet AddCountry(CountryRequest data)
        {
            DataSet ds = new DataSet();
            try
            {
                ConnectionCheck();
                SqlCommand dCmd = new SqlCommand("Add_Update_Country_SP", new SqlConnection(Configuration.GetConnectionString("DefaultConnection")));
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add(new SqlParameter("@UserId", data.UserId));
                dCmd.Parameters.Add(new SqlParameter("@Country_Id", data.Country_Id));
                dCmd.Parameters.Add(new SqlParameter("@Country_Group_Id", data.Country_Group_Id));
                dCmd.Parameters.Add(new SqlParameter("@Country_Name", data.Country_Name));
                dCmd.Parameters.Add(new SqlParameter("@Country_Status", data.Country_Status));
                using (SqlDataAdapter sda = new SqlDataAdapter(dCmd))
                {
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DataSet CountryList(int GroupId)
        {
            DataSet ds = new DataSet();
            try
            {
                ConnectionCheck();
                SqlCommand dCmd = new SqlCommand("GetCountryList", new SqlConnection(Configuration.GetConnectionString("DefaultConnection")));
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add(new SqlParameter("@GroupId", GroupId));
                using (SqlDataAdapter sda = new SqlDataAdapter(dCmd))
                {
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DataSet CountryGroupList()
        {
            DataSet ds = new DataSet();
            try
            {
                ConnectionCheck();
                SqlCommand dCmd = new SqlCommand("GetCountryGroupList", new SqlConnection(Configuration.GetConnectionString("DefaultConnection")));
                dCmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataAdapter sda = new SqlDataAdapter(dCmd))
                {
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Dictionary<string, object>> ConvertDataDStoJSON(DataSet ds)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            try
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return rows;
        }


    }
}
