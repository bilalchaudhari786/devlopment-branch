﻿using API.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API.Implementation
{
    public class DBContext
    {
        private IConfiguration Configuration;
        public DBContext(IConfiguration iconfiguration)
        {
            Configuration = iconfiguration;
        }
        public void ConnectionCheck()
        {
            string connString = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection connection = new SqlConnection(connString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
            }
        }
        
        public string AddIPLog()
        {
            string result = "Fail";
            try
            {
                ConnectionCheck();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return result;
        }
    }
}
