﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Implementation;
using Microsoft.Extensions.Configuration;
using API.Model;

namespace API.Controllers
{
    [Route("api/Country")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public CountryController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }
        
        [Route("CreateGroup")]
        [HttpPost]
        public IActionResult CreateGroup(CountryGroupRequest data)
        {
            CountryRepo db = new CountryRepo(_iconfiguration);
            var Responsedata = db.ConvertDataDStoJSON(db.AddCountryGroup(data));
            return Ok(Responsedata);
        }
        [Route("CountryGroup_List")]
        [HttpGet]
        public IActionResult CountryGroup_List()
        {
            CountryRepo db = new CountryRepo(_iconfiguration);
            var Responsedata = db.ConvertDataDStoJSON(db.CountryGroupList());
            return Ok(Responsedata);
        }
        [Route("AddCountry")]
        [HttpPost]
        public IActionResult AddCountry(CountryRequest data)
        {
            CountryRepo db = new CountryRepo(_iconfiguration);
            var Responsedata = db.ConvertDataDStoJSON(db.AddCountry(data));
            return Ok(Responsedata);
        }
        [Route("CountryAllList")]
        [HttpGet]
        public IActionResult CountryAllList()
        {
            CountryRepo db = new CountryRepo(_iconfiguration);
            var Responsedata = db.ConvertDataDStoJSON(db.CountryList(0));
            return Ok(Responsedata);
        }
        [Route("GroupByCountryList")]
        [HttpGet]
        public IActionResult GroupByCountryList(int GroupId)
        {
            CountryRepo db = new CountryRepo(_iconfiguration);
            var Responsedata = db.ConvertDataDStoJSON(db.CountryList(GroupId));
            return Ok(Responsedata);
        }
    }
}
